class Battle {
    static get STATUS_PRE_GAME() {
        return 'status_pre_game';
    }

    static get STATUS_IN_PROGRESS() {
        return 'status_in_progress';
    }

    static get STATUS_GAME_WON() {
        return 'status_game_won';
    }

    get state() {
        return this._state;
    }

    set state(value) {
        this._state = value;
        return this;
    }

    get logger() {
        return this._logger;
    }

    set logger(value) {
        this._logger = value;
        return this;
    }

    constructor(state, logger) {
        this._state = state;
        this._logger = logger;
    }

    start() {
        const xhr = new XMLHttpRequest();
        xhr.open('get', '/battle/start');
        xhr.addEventListener(
            'load',
            (event) => {
                let response = null;
                try {
                    response = JSON.parse(event.target.responseText);
                } catch(e) {
                    //console.log(e);
                    alert('There was an error while parsing server response data.');
                    return;
                }

                const responseBattle = response ? response['data'] ?  response['data']['battle'] : null : null;
                if(typeof responseBattle === 'object') {
                    responseBattle.config.x = parseInt(responseBattle.config.x);
                    responseBattle.config.y = parseInt(responseBattle.config.y);
                    responseBattle.round = parseInt(responseBattle.round);
                    this.state = new BattleState(responseBattle.config, responseBattle.sizes, responseBattle.status, responseBattle.round);
                    let startFightWrapper = document.createElement('div');
                    startFightWrapper.className = 'start-fight-wrapper';
                    let startFight = document.createElement('div');
                    startFight.className = 'start-fight';
                    startFight.style.fontSize = '45vw';
                    startFight.innerHTML = 'Fight!!!';
                    startFightWrapper.appendChild(startFight);
                    document.body.appendChild(startFightWrapper);

                    let fightIntervalFlickerState = true;
                    let fightIntervalFlicker;
                    fightIntervalFlicker = setInterval(
                        () => {
                            if (fightIntervalFlickerState) {
                                startFight.style.color = '#FFF';
                                fightIntervalFlickerState = false;
                            } else {
                                startFight.style.color = '';
                                fightIntervalFlickerState = true;
                            }
                        },
                        100
                    );
                    let fightInterval;
                    fightInterval = setInterval(
                        () => {
                            const startFightFontSizeUnits = parseInt(startFight.style.fontSize.replace(/[a-zA-Z%]+/, ''));
                            if (startFightFontSizeUnits > 10) {
                                startFight.style.fontSize = (startFightFontSizeUnits - 5) + startFight.style.fontSize.match(/[a-zA-Z%]+/, '')[0];
                            } else {
                                clearInterval(fightIntervalFlicker);
                                startFight.style.color = '#FF0000';
                                startFight.style.textShadow = '0vw 0vw 2vw #FFFF00';
                                clearInterval(fightInterval);
                                setTimeout(
                                    () => {
                                        document.body.removeChild(startFightWrapper);
                                        this.logger.logMessage(
                                            `<div class="font-size-1vw line-height-1vw">
The battle has begun! Grid size: ${this.state.config.x}x${this.state.config.y}
| Enemy battleships: <br>
| Pick your first target coordinates!<br>
Round #${this.state.round} begins.
</div>`
                                        );
                                    },
                                    1000
                                );
                            }
                        },
                        200
                    );
                }
            }
        );
        xhr.addEventListener(
            'error',
            function(event) {
                //console.log(event);
                alert('There was an error while fetching data from the server.');
            }
        );
        xhr.send();
    }

    shoot(x, y) {
        document.getElementById('coordinate-' + x + '-' + y).className = document.getElementById('coordinate-' + x + '-' + y).className.replace(new RegExp('\\s*shot-at'), '') + ' shot-at';;

        const xhr = new XMLHttpRequest();
        xhr.open('post', '/battle/shoot');
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.addEventListener(
            'load',
            (event) => {
                let response = null;
                try {
                    response = JSON.parse(event.target.responseText);
                } catch(e) {
                    //console.log(e);
                    alert('There was an error while parsing server response data.');
                    return;
                }
                switch(response.data.result.outcome) {
                    case ShootResult.OUTCOME_FAILURE_GAME_NOT_IN_PROGRESS:
                        break;
                    case ShootResult.OUTCOME_FAILURE_INVALID_COORDINATES:
                        this.logger.logError('Invalid coordinates.');
                        break;
                    case ShootResult.OUTCOME_FAILURE_ALREADY_SHOT_AT:
                        this.logger.logError(`You have already fired a shot @ coordinates ${x}x${y}.`);
                        break;
                    case ShootResult.OUTCOME_SUCCESS_MISS:
                        document.getElementById('coordinate-' + x + '-' + y).className = document.getElementById('coordinate-' + x + '-' + y).className.replace(new RegExp('\\s*shot-miss'), '') + ' shot-miss';
                        this.logger.logMessage(
                            `<div class="font-size-1vw line-height-1vw">
Your shot @ coordinates ${x}x${y} was a miss. :(
</div>`
                        );
                        this.logger.logMessage(
                            `<div class="font-size-1vw line-height-1vw">
Round #${response.data.result.round + 1} begins.
</div>`
                        );
                        break;
                    case ShootResult.OUTCOME_SUCCESS_HIT:
                        document.getElementById('coordinate-' + x + '-' + y).className = document.getElementById('coordinate-' + x + '-' + y).className.replace(new RegExp('\\s*shot-hit'), '') + ' shot-hit';
                        this.logger.logMessage(
                            `<div class="font-size-1vw line-height-1vw">
Your shot @ coordinates ${x}x${y} was a hit! o7<br>
</div>`
                        );
                        if(response.data.result.shipSunken) {
                            this.logger.logMessage(
                                `<div class="font-size-1vw line-height-1vw">
You sank a ship! Pew pew!
</div>`
                            );
                        }
                        if(response.data.result.victory) {
                            this.logger.logMessage(
                                `<div class="font-size-1vw line-height-1vw">
Congratulations! You have destroyed all of the enemy ships in ${response.data.result.round} rounds.
</div>`
                            );
                        } else {
                            this.logger.logMessage(
                                `<div class="font-size-1vw line-height-1vw">
Round #${response.data.result.round + 1} begins.
</div>`
                            );
                        }
                        break;
                    default:
                        break;
                }
            }
        );
        xhr.addEventListener(
            'error',
            function(event) {
                //console.log(event);
                alert('There was an error while fetching data from the server.');
            }
        );
        xhr.send('x=' + x + '&y=' + y);
    }

    cheat() {
        const xhr = new XMLHttpRequest();
        xhr.open('get', '/battle/cheat');
        xhr.addEventListener(
            'load',
            (event) => {
                let response = null;
                try {
                    response = JSON.parse(event.target.responseText);
                } catch(e) {
                    //console.log(e);
                    alert('There was an error while parsing server response data.');
                    return;
                }

                const finalPositions = response ? response['data'] ?  response['data']['finalPositions'] : [] : [];
                finalPositions.forEach(
                    (element) => {
                        if (document.getElementById('wrapper').className.match(new RegExp('\\s*cheat'))) {
                            document.getElementById('wrapper').removeChild(document.getElementById('sprite-' + element.size + '-' + element.index));

                            element.coordinates.forEach(
                                function (element) {
                                    let dx = element['x'];
                                    let dy = element['y'];
                                    document.getElementById('cell-' + dx + '-' + dy).className = document.getElementById('cell-' + dx + '-' + dy).className.replace(new RegExp('\\s*ship' + this.size + this.index), '');
                                }.bind(element)
                            );
                        } else {
                            const sprite = document.createElement('div');
                            sprite.id = 'sprite-' + element.size + '-' + element.index;
                            sprite.className = 'sprite sprite-' + element.size + '-' + element.index + ' size-' + element.size;
                            sprite.style.width = String((100 * element.size * element.perspective / (this.state.config.x + 1))) + '%';
                            sprite.style.height = String(100 / (this.state.config.y + 1)) + '%';
                            sprite.style.left = String(100 * (element.x) / (this.state.config.x)) + '%';
                            sprite.style.top = String(100 * (element.y) / (this.state.config.y)) + '%';
                            document.getElementById('wrapper').appendChild(sprite);
                            sprite.style.transform = 'rotate(' + element.deg + 'deg)';

                            element.coordinates.forEach(
                                function (element) {
                                    let dx = element['x'];
                                    let dy = element['y'];
                                    document.getElementById('cell-' + dx + '-' + dy).className = document.getElementById('cell-' + dx + '-' + dy).className.replace(new RegExp('\\s*ship' + this.size + this.index), '') + ' ship' + this.size + this.index;
                                }.bind(element)
                            );
                        }
                    }
                );

                if (document.getElementById('wrapper').className.match(new RegExp('\\s*cheat'))) {
                    document.getElementById('wrapper').className = document.getElementById('wrapper').className.replace(new RegExp('\\s*cheat'), '');
                } else {
                    document.getElementById('wrapper').className = document.getElementById('wrapper').className.replace(new RegExp('\\s*cheat'), '') + ' cheat';
                }
            }
        );
        xhr.addEventListener(
            'error',
            function(event) {
                //console.log(event);
                alert('There was an error while fetching data from the server.');
            }
        );
        xhr.send();
    }
}
