class ShootResult {
    static get OUTCOME_FAILURE_GAME_NOT_IN_PROGRESS() {
        return 'outcome_failure_game_not_in_progress';
    }

    static get OUTCOME_FAILURE_INVALID_COORDINATES() {
        return 'outcome_failure_invalid_coordinates';
    }

    static get OUTCOME_FAILURE_ALREADY_SHOT_AT() {
        return 'outcome_failure_already_shot_at';
    }

    static get OUTCOME_SUCCESS_MISS() {
        return 'outcome_success_miss';
    }

    static get OUTCOME_SUCCESS_HIT() {
        return 'outcome_success_hit';
    }
}
