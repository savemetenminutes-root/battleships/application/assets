class BattleState {
    get config() {
        return this._config;
    }

    set config(value) {
        this._config = value;
    }

    get sizes() {
        return this._sizes;
    }

    set sizes(value) {
        this._sizes = value;
    }

    get status() {
        return this._status;
    }

    set status(value) {
        this._status = value;
    }

    get round() {
        return this._round;
    }

    set round(value) {
        this._round = value;
    }

    constructor(config, sizes, status, round) {
        this._config = config;
        this._sizes = sizes;
        this._status = status;
        this._round = round;
    }
}
