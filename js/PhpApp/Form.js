class Form
{
    static getCheckedRadio(name, source)
    {
        source = source || document;
        let checkboxes = source.getElementsByName(name);
        for(let i=0; i < checkboxes.length; i++) {
            if(checkboxes[i].checked) {
                return checkboxes[i];
            }
        }

        return false;
    }

    static getFormData(form)
    {
        let query = [];
        let inputs = form.getElementsByTagName('input');
        let textareas = form.getElementsByTagName('textarea');
        let selects = form.getElementsByTagName('select');

        let checkboxes = [];
        let radios = [];
        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].name) {
                if (!inputs[i].disabled) {
                    switch (inputs[i].type.toLowerCase()) {
                        case 'hidden' :
                        case 'text' :
                            query[inputs[i].name] =  inputs[i].value;
                            break;
                        case 'radio' :
                            radios[inputs[i].name] = inputs[i];
                            break;
                        case 'checkbox' :
                            checkboxes[inputs[i].name] = inputs[i];
                            break;
                    }
                }
            }
        }

        radios = radios.values();
        checkboxes = checkboxes.values();

        for (const i of radios) {
            query[radios.name] = getCheckedRadio(radios.name).value;
        }
        for (let i =0; i < checkboxes.length; i++) {
            query[checkboxes[i].name] = [];
            if (checkboxes[i].checked) {
                query[checkboxes[i].name].push(checkboxes[i].value);
            }
        }
        for (let i = 0; i < textareas.length; i++) {
            if (textareas[i].name) {
                if (!textareas[i].disabled) {
                    query[textareas[i].name] = textareas[i].value;
                }
            }
        }
        for (let i = 0; i < selects.length; i++) {
            if (selects[i].name) {
                if (!selects[i].disabled) {
                    query[selects[i].name] = selects[i].value;
                }
            }
        }

        return query;
    }
}
