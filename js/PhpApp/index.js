let battle;
document.addEventListener(
    'DOMContentLoaded',
    () => {
        const xhr = new XMLHttpRequest();
        xhr.open('get', '/battle');
        xhr.addEventListener(
            'load',
            (event) => {
                let response = null;
                if(event.target.responseText === '') {
                    return;
                }
                try {
                    response = JSON.parse(event.target.responseText);
                } catch(e) {
                    return;
                }

                const responseBattle = response ? response['data'] ? response['data']['battle'] ? response['data']['battle'] : null : null : null;
                if(typeof responseBattle === 'object' && responseBattle !== null) {
                    responseBattle.config.x = parseInt(responseBattle.config.x);
                    responseBattle.config.y = parseInt(responseBattle.config.y);
                    responseBattle.round = parseInt(responseBattle.round);
                    const battleState = new BattleState(responseBattle.config, responseBattle.sizes, responseBattle.status, responseBattle.round);
                    battle = new Battle(battleState, new Logger(responseBattle.config));
                    document.getElementById('input-button-cheat')
                        .addEventListener(
                            'click',
                            battle.cheat.bind(battle)
                        );

                    document.getElementById('input-button-shoot')
                        .addEventListener(
                            'click',
                            () => {
                                const x = document.getElementById('input-x').value;
                                const y = document.getElementById('input-y').value;
                                battle.shoot(x, y);
                            }
                        );
                    for (let x = 1; x <= battle.state.config.x; x++) {
                        for (let y = 1; y <= battle.state.config.y; y++) {
                            document.getElementById('coordinate-' + x + '-' + y)
                                .addEventListener(
                                    'click',
                                    battle.shoot.bind(battle, x, y)
                                );
                        }
                    }
                } else {

                }
            }
        );
        xhr.addEventListener(
            'error',
            function(event) {
                //console.log(event);
                alert('There was an error while fetching data from the server.');
            }
        );
        xhr.send();
    }
);
